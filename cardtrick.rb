class Card

	#attr_accessor :value, :suit

	def initialize(val, s)
		if val == 'A' then
			@value = 1
		elsif val == 'J' then
			@value = 11
		elsif val == 'Q'
			@value = 12
		elsif val == 'K' then
			@value = 13
		else
			@value = val.to_i
		end
		@suit = s
	end

	def value
		if @value == 1 then
			return 'A'
		elsif @value == 11 then
			return 'J'
		elsif @value == 12 then
			return 'Q'
		elsif @value == 13 then
			return 'K'
		else
			return @value
		end
	end
	
	def value_num
		return @value
	end
	
	def suit
		return @suit
	end
	
	def suit_value
	
		if @suit == "Clubs" then
			@suit_value = 1
		elsif @suit == "Spades" then
			@suit_value = 2
		elsif @suit == "Hearts" then
			@suit_value = 3
		elsif @suit == "Diamonds" then
			@suit_value = 4
		end
		
		return @suit_value
	end
	
	def setorder(o)
		@order = o
	end
	
	def getorder
		return @order
	end
	
end

puts "Accepted values: A, 2, 3, 4, 5, 6, 7, 8, 9, 10, J, Q, K"
puts "Acceptes suits: Clubs, Spades, Hearts, Diamonds"

print "What's the first card's value? "
value = gets.chomp
print "What's the first card's suit? "
suit = gets.chomp
card1 = Card.new(value, suit)
puts ""

print "What's the second card's value? "
value = gets.chomp
print "What's the second card's suit? "
suit = gets.chomp
card2 = Card.new(value, suit)
puts ""

print "What's the third card's value? "
value = gets.chomp
print "What's the third card's suit? "
suit = gets.chomp
card3 = Card.new(value, suit)
puts ""

print "What's the fourth card's value? "
value = gets.chomp
print "What's the fourth card's suit? "
suit = gets.chomp
card4 = Card.new(value, suit)
puts ""

print "Positive rotation or negative rotation? (+ or -) "
rotation = gets.chomp

puts "\nYour four cards: #{card1.value.to_s + " of " + card1.suit}, #{card2.value.to_s + " of " + card2.suit}, #{card3.value.to_s + " of " + card3.suit}, #{card4.value.to_s + " of " + card4.suit}."

card_suit = card1.suit
puts "The 5th card's suit is the #{card_suit} suit."

initial_value = card1.value
puts "The base value is #{initial_value}"

card_array = [card2, card3, card4]

card_array_sorted = card_array.sort_by{|c| [c.value_num, c.suit_value]}

i = 0
while i < 3 do
	j = 0
	while j < 3 do
		if card_array[i] == card_array_sorted[j] then
			card_array[i].setorder(j + 1)
		end
		j += 1
	end
	i += 1
end

val_order = Array.new(3)

i = 0
while i < 3 do
	val_order[i] = card_array[i].getorder
	i += 1
end

offset = 0

if val_order[0] == 1 then

	if val_order[1] == 2 then
		offset = 1
	elsif val_order[1] == 3 then
		offset = 2
	end
	
elsif val_order[0] == 2 then

	if val_order[1] == 3 then
		offset = 3
	elsif val_order[1] == 1 then
		offset = 4
	end	
	
elsif val_order[0] == 3 then

	if val_order[1] == 1 then
		offset = 5
	elsif val_order[1] == 2 then
		offset = 6
	end
	
end

if rotation == '+' then
	card_value = initial_value.to_i + offset.to_i
elsif rotation == '-' then
	if initial_value.to_i <= offset.to_i then
		card_value = 13
		if initial_value.to_i < offset.to_i then
			offset.to_i -= initial_value.to_i
			card_value = 13 - (offset.to_i + 1)
		end
	else
		card_value = initial_value.to_i - offset.to_i
	end
end
puts "The card you hold is the #{card_value.to_s + " of " + card_suit}."

#123 = 1
#132 = 2

#231 = 3
#213 = 4

#312 = 5
#321 = 6